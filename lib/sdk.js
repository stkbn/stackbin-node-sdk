require('es-nodeify');

var _ = require('lodash');

const DEFAULTS = {
    version: '2015-08-02',
    username: '',
    password: '',
    api_host: 'api.stkbn.com',
    region: 'ap-southeast-1'
};

var SDK = module.exports = function (config) {

    var transport = require('./transport')(config);

    var sdk = {

        _transport: transport,

        /**
         * @name SDK.config
         * @param {Object} config
         * @returns {Promise}
         */
        config: function sdk_config(config) {
            config = _.defaults(config, DEFAULTS);
        },

        /**`
         * @name SDK.listAccounts
         * @param {Function} [cb]
         * @returns {Promise}
         */
        listAccounts: function sdk_listAccounts(cb) {
            return sdk._transport.get('accounts').nodeify(cb);
        },

        /**
         * @name SDK.getAccount
         * @param {Object} params
         * @param {String} [params.account_id]
         * @param {String} [params.domain]
         * @param {Function} [cb]
         * @returns {Promise}
         */
        getAccount: function sdk_getAccount(params, cb) {
            return sdk._transport.get(`accounts/${params.domain}`).nodeify(cb);
        },

        /**
         * @name SDK.getProject
         * @param {Object} params
         * @param {String} params.project_id
         * @param {Function} [cb]
         * @returns {Promise}
         */
        getProject: function sdk_getProject(params, cb) {
            return sdk._transport.get(`accounts/${params.domain}/project/${params.slug}`).nodeify(cb);
        },

        /**
         * @name SDK.getProjects
         * @param {Object} params
         * @param {String} params.account_id
         * @param {Function} [cb]
         * @returns {Promise}
         */
        getProjects: function sdk_getProjects(params, cb) {
            return sdk._transport.get('projects').nodeify(cb);
        },

        /**
         * @name SDK.getReport
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        getReport: function sdk_getReport(params, cb) {
            return sdk._transport.get(`accounts/${params.domain}/project/${params.slug}/report/${params.report_id}`).nodeify(cb);
        },

        /**
         * @name SDK.addUser
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        addUser: function sdk_addUser(params, cb) {
            return sdk._transport.post(`users`, params).nodeify(cb);
        },

        /**
         * @name SDK.updateInvite
         * @param {Object} params
         * @param {Function} [cb]
         * @returns {Promise}
         */
        updateInvite: function sdk_updateInvite(params, cb) {
            var path = `accounts/${params.domain}/invites/${params.invite_id}`;
            delete params.domain;
            delete params.invite_id;
            return sdk._transport.put(path, params).nodeify(cb);
        }
    };

    sdk.config(config || {});

    return sdk;

};

module.exports = SDK;
