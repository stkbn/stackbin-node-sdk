'use strict';

var _ = require('lodash');
var restler = require('restler');
var debug = require('debug')('stackbin:sdk:transport');
var manifest = require('../package.json');

// This is for 429s only
const BACKOFF = 1000;
const MAX_BACKOFF = 1000 ^ 5;

// This is for timeout only
const MAX_RETRIES = 5;
const TIMEOUT = 3000;

// REST DEFAULTS
var DEFAULTS = {
    timeout: TIMEOUT,
    headers: {
        'User-Agent': manifest.name + '/' + manifest.version
    }
};

if (process.env.npm_package_name) {
    DEFAULTS.headers['User-Agent'] = process.env.npm_package_name + '/' + (process.env.npm_package_version || '0.0') + ' (' + DEFAULTS.headers['User-Agent'] + ')';
}

function rest(method, resource, payload, config) {

    var backoff = BACKOFF;
    var auth_attempted = false;
    var retries = 0;

    return new Promise(function (resolve, reject) {

        var url = `https://${config.api_host}`;

        debug('Making REST call', method, url, resource);

        var rest_config = _.defaultsDeep(DEFAULTS, {
            headers: {}
        });

        if (config.token) {
            debug('Using token', '...' + config.token.substr(-7));
            rest_config.headers.Authorization = 'Bearer ' + config.token;
        }

        debug('Using config', rest_config);

        debug('Payload', method, payload);

        var restler_method;
        if (method === 'get') {
            payload = rest_config;
            rest_config = undefined;
            restler_method = method;
        } else {
            restler_method = method + 'Json';
        }

        restler[restler_method](`${url}/${resource}`, payload, rest_config)
            .on('success', function (data) {
                debug('Gotcha!', data);
                backoff = BACKOFF;
                resolve(data);
            })
            .on('fail', function (data, response) {

                debug('Fail Event. Response:', response.statusCode, data);

                var retry = this.retry.bind(this);

                var err;

                switch (response.statusCode) {
                    case 400:
                        err = new Error(data.error.message);
                        err.statusCode = response.statusCode;
                        err.name = data.error.type;
                        reject(err);
                        break;

                    case 401:
                        if (!auth_attempted && !rest_config.token && config.username && config.password) {
                            debug('Auth not yet attempted, and token is empty');
                            auth_attempted = true;

                            restler.post(`${url}/tokens`, _.defaultsDeep({}, config, DEFAULTS))
                                .on('success', function (data, response) {
                                    debug('Success! Retrying with token...', data);
                                    config.token = data.token;
                                    rest_config.headers.Authorization = 'Bearer ' + config.token;
                                    retry(0);
                                })
                                .on('fail', function (data, response) {
                                    debug('Fail! ', data);
                                    err = new Error(`${data.error.message} (${data.error.type})`);
                                    err.statusCode = response.statusCode;
                                    err.name = 'TransportAuthenticationError';
                                    reject(err);
                                })
                                .on('error', function (err) {
                                    debug('Error! ', err);
                                    err.name = 'TransportHttpError';
                                    reject(err);
                                });

                        } else {
                            err = new Error(data.error.message);
                            err.name = 'TransportAuthenticationError';
                            err.statusCode = response.statusCode;
                            reject(err);
                        }

                        break;

                    case 429:

                        if (backoff < MAX_BACKOFF) {
                            backoff *= 2;
                            debug('Retrying in ' + backoff);
                            retry(backoff);
                        } else {
                            err.name = 'TransportCapacityError';
                            reject(err);
                        }

                        break;

                    default:
                        err = new Error(response.status || response.statusCode);
                        err.statusCode = response.statusCode;
                        reject(err);
                        break;
                }

            })
            .on('error', function (err, response) {
                err.name = 'TransportHttpError';
                reject(err);
            })
            .on('timeout', function () {

                if (++retries < MAX_RETRIES) {
                    debug('Retrying in ' + TIMEOUT);
                    this.retry(TIMEOUT);
                } else {
                    var err = new Error('Timeout during postJson  of ' + url);
                    err.name = 'TransportTimeoutError';
                    reject(err);
                }

            });
    });

}

var transport = function transport(config) {

    return {
        get: function (path) {
            return rest('get', path, null, config);
        },

        put: function (path, payload) {
            return rest('put', path, payload, config);
        },

        post: function (path, payload) {
            return rest('post', path, payload, config);
        },

        patch: function (path, payload) {
            return rest('patch', path, payload, config);
        },

        delete: function (path) {
            return rest('delete', path, null, config);
        }
    };

};

module.exports = transport;
